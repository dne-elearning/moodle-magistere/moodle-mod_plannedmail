<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This file contains the forms to create and edit an instance of this module
 *
 * @package    mod_plannedmail
 * @copyright  2023 DNE - Ministere de l'Education Nationale
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/course/moodleform_mod.php');

/**
 * Moodleform class.
 *
 * @package    mod_plannedmail
 * @copyright  2023 DNE - Ministere de l'Education Nationale
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class mod_plannedmail_mod_form extends moodleform_mod {

    /**
     * Called to define this moodle form
     *
     * @return void
     */
    public function definition() {

        global $COURSE, $CFG, $PAGE;
        $mform =& $this->_form;
        // Make sure completion and restriction is enabled.
        if (empty($CFG->enablecompletion) || empty($CFG->enableavailability)) {
            print_error('mustenablecompletionavailability', 'mod_plannedmail');
        }
        // Adding the "general" fieldset, where all the common settings are shown.
        $mform->addElement('header', 'general', get_string('general', 'form'));
        if (!$COURSE->enablecompletion) {
            $coursecontext = context_course::instance($COURSE->id);
            if (has_capability('moodle/course:update', $coursecontext)) {
                $mform->addElement('static', 'completionwillturnon', get_string('completion', 'plannedmail'),
                                   get_string('completionwillturnon', 'plannedmail'));
            }
        }

        // Adding the standard "name" field.
        $mform->addElement('text', 'name', get_string('plannedmailname', 'plannedmail'), array('size' => '64'));
        $mform->setType('name', PARAM_TEXT);
        $mform->addRule('name', null, 'required', null, 'client');
        $mform->addRule('name', get_string('maximumchars', '', 255), 'maxlength', 255, 'client');

        // Adding the rest of plannedmail settings, spreeading all them into this fieldset
        // or adding more fieldsets ('header' elements) if needed for better logic.
        $mform->addElement('header', 'plannedmailfieldset', get_string('plannedmailfieldset', 'plannedmail'));
        $mform->setExpanded('plannedmailfieldset', true);

        // mail issuer
        $mform->addElement('select', 'issuerid', get_string('mailissuer', 'plannedmail'), self::get_plannedmail_issuers_options());
        

        // mail subject is the name of the plannedmail activity now, keeping those lines in case we want the subject back
        // $mform->addElement('text', 'mailsubject', get_string('mailsubject', 'plannedmail'), array('size' => '64'));
        // $mform->setType('mailsubject', PARAM_TEXT);
        // $mform->addRule('mailsubject', get_string('maximumchars', '', 255), 'maxlength', 255, 'client');
        // $mform->addHelpButton('mailsubject', 'mailsubject', 'plannedmail');
        $mform->addElement('editor', 'mailcontent', get_string('mailcontent', 'plannedmail'), null, null);
        $mform->setDefault('mailcontent', get_string('mailcontentdefaultvalue', 'plannedmail'));
        $mform->setType('mailcontent', PARAM_RAW);

        // mail issuer checkbox
        $mform->addElement('advcheckbox', 'sendmailtoissuer', '', get_string('sendmailtoissuer','plannedmail'), array(), array(0, 1));
        $mform->hideIf('sendmailtoissuer', 'issuerid', 'eq', PLANNEDMAIL_NOREPLY_USER);

        // mail send subject
        $mform->addElement('text', 'mailtoissuersubject',
            get_string('mailtoissuersubject', 'plannedmail'), array('size' => '64'));
        $mform->setType('mailtoissuersubject', PARAM_TEXT);
        $mform->addRule('mailtoissuersubject', get_string('maximumchars', '', 255), 'maxlength', 255, 'client');
        $mform->addHelpButton('mailtoissuersubject', 'mailtoissuersubject', 'plannedmail');
        $mform->hideIf('mailtoissuersubject','sendmailtoissuer', 'notchecked');
        $mform->hideIf('mailtoissuersubject','issuerid', 'eq', PLANNEDMAIL_NOREPLY_USER);    

        // mail issuer content
        $mform->addElement('editor', 'mailtoissuercontent', get_string('mailtoissuercontent', 'plannedmail'), null, null);
        $mform->setDefault('mailtoissuercontent', get_string('mailtoissuercontentdefaultvalue', 'plannedmail'));
        $mform->setType('mailtoissuercontent', PARAM_RAW);
        $mform->addHelpButton('mailtoissuercontent', 'mailtoissuercontent', 'plannedmail');
        $mform->hideIf('mailtoissuercontent','sendmailtoissuer', 'notchecked');
        $mform->hideIf('mailtoissuercontent','issuerid', 'eq', PLANNEDMAIL_NOREPLY_USER);

        // selector between delay and delay + time mode
        $messagedelayoptions = array();
        $messagedelayoptions[PLANNEDMAIL_DELAYMODE_DELAY] = get_string('delay','plannedmail');
        $messagedelayoptions[PLANNEDMAIL_DELAYMODE_DELAYANDTIME] = get_string('delayandprecisetime','plannedmail');
        $mform->addElement('select', 'messagedelaymode', get_string('messagedelay_mode', 'plannedmail'), $messagedelayoptions);

        // Add a group of controls to specify after how long an email should be sent.
        // delay mode
        $maildelay = array();
        $delayperiods = array();
        $delayperiods[60] = get_string('minutes', 'plannedmail');
        $delayperiods[3600] = get_string('hours', 'plannedmail');
        $delayperiods[86400] = get_string('days', 'plannedmail');
        $delayperiods[604800] = get_string('weeks', 'plannedmail');
        $maildelay[] = $mform->createElement('text', 'emailperiodcount', '', array('class="emailperiodcount"'));
        $maildelay[] = $mform->createElement('select', 'emailperiod', '', $delayperiods);
        $mform->addGroup($maildelay, 'maildelay', get_string('maildelay', 'plannedmail'), array(' '), false);
        $mform->addHelpButton('maildelay', 'maildelay', 'plannedmail');
        $mform->setType('emailperiodcount', PARAM_INT);
        $mform->setDefault('emailperiodcount', '1');
        $mform->setDefault('emailperiod', '604800');
        $mform->hideif('maildelay','messagedelaymode', 'neq', PLANNEDMAIL_DELAYMODE_DELAY);

        // delay + time mode
        $timeperiods = array();
        $timeperiods[86400] = get_string('days', 'plannedmail');
        $timeperiods[604800] = get_string('weeks', 'plannedmail');
        $delaygroup =array();
        $mailtime= array();
        $delaygroup[] = $mform->createElement('text', 'mailtimecount', '', array('class="emailperiodcount"'));
        $delaygroup[] = $mform->createElement('select', 'mailtimedelay', '', $timeperiods);
        $mailtime[] = $mform->createElement('group', 'delaygroup', get_string('In', 'plannedmail').'&nbsp;', $delaygroup, '', false);
        $mailtime[] = $this->add_time_selector('hours', 'minutes', 'mailtime', get_string('at', 'plannedmail').'&nbsp;');
        $mform->setType('mailtimecount', PARAM_INT);
        $mform->addGroup($mailtime, 'maildelayandtime', get_string('mailtimedelay', 'plannedmail'), array(' '), false);
        $mform->hideif('maildelayandtime','messagedelaymode', 'neq', PLANNEDMAIL_DELAYMODE_DELAYANDTIME);

        
        $mform->addElement('advcheckbox', 'suppressemail', get_string('suppressemail', 'plannedmail'));
        $mform->addHelpbutton('suppressemail', 'suppressemail', 'plannedmail');
        $truemods = get_fast_modinfo($COURSE->id);
        $mods = array();
        $mods[0] = get_string('nosuppresstarget', 'plannedmail');
        foreach ($truemods->cms as $mod) {
            $mods[$mod->id] = $mod->name;
        }
        $mform->addElement('select', 'suppresstarget', get_string('suppresstarget', 'plannedmail'), $mods);
        $mform->hideif('suppresstarget', 'suppressemail', 'notchecked');
        $mform->addHelpbutton('suppresstarget', 'suppresstarget', 'plannedmail');

        // Add standard elements, common to all modules.
        $this->standard_coursemodule_elements();
        if ($mform->elementExists('completion')) {
            $mform->setDefault('completion', COMPLETION_TRACKING_NONE);
        }
        // Hide some elements not relevant to this activity (student visibility)
        if ($mform->elementExists('visible')) {
            $mform->removeElement('visible');
            $mform->addElement('hidden', 'visible', 0);
            $mform->setType('visible', PARAM_INT);
            if ($mform->elementExists('visibleoncoursepage')) {
                $mform->removeElement('visibleoncoursepage');
            }
            $mform->addElement('hidden', 'visibleoncoursepage', 1);
            $mform->setType('visibleoncoursepage', PARAM_INT);
        }

        // Add standard buttons, common to all modules.
        $this->add_action_buttons();

        $PAGE->requires->js_call_amd('mod_plannedmail/form', 'init');
    }

    /**
     * Load in existing data as form defaults
     *
     * @param stdClass|array $toform object or array of default values
     */
    public function set_data($toform) {
        global $CFG;
        // $istotara = false;
        // if (file_exists($CFG->dirroot.'/totara')) {
        //     $istotara = true;
        // }
        // Form expects durations as a number of periods eg 5 minutes.
        // Process dbtime (seconds) into form-appropraite times.
        if (!empty($toform->duration)) {
            list ($periodcount, $period) = plannedmail_get_readable_duration($toform->duration);
            $toform->period = $period;
            $toform->periodcount = $periodcount;
            unset($toform->duration);
        }
        
        if (!empty($toform->mailtime)) {
            list ($periodcount, $period) = plannedmail_get_readable_duration($toform->maildelay);
            $toform->mailtimedelay = $period;
            $toform->mailtimecount = $periodcount;
            $toform->hours = intdiv($toform->mailtime, 3600);
            $toform->minutes =  intdiv($toform->mailtime % 3600, 60);
            $toform->messagedelaymode = PLANNEDMAIL_DELAYMODE_DELAYANDTIME;
            unset($toform->mailtime);
            unset($toform->maildelay);
        } elseif (!empty($toform->maildelay)) {
            list ($periodcount, $period) = plannedmail_get_readable_duration($toform->maildelay);
            $toform->emailperiod = $period;
            $toform->emailperiodcount = $periodcount;
            $toform->messagedelaymode = PLANNEDMAIL_DELAYMODE_DELAY;
            unset($toform->maildelay);
        }

        if (empty($toform->suppresstarget)) {
            // There is no target activity specified.
            // Configure the box to have this dropdown disabled by default.
            $toform->suppressemail = 0;
        } else {
            // There is a target activity specified, enable the target selector so that the user can change it if desired.
            $toform->suppressemail = 1;
        }
        
        if (!isset($toform->mailcontent)) {
            $toform->mailcontent = get_string('mailcontentdefaultvalue', 'plannedmail');
        }
        if (!isset($toform->mailcontentformat)) {
            $toform->mailcontentformat = 1;
        }
        $toform->mailcontent = array('text' => $toform->mailcontent, 'format' => $toform->mailcontentformat);

        if (!isset($toform->mailtoissuercontent)) {
            $toform->mailtoissuercontent = get_string('mailtoissuercontentdefaultvalue', 'plannedmail');
        }
        if (!isset($toform->mailtoissuercontentformat)) {
            $toform->mailtoissuercontentformat = 1;
        }
        $toform->mailtoissuercontent = array('text' => $toform->mailtoissuercontent, 'format' => $toform->mailtoissuercontentformat);

        // Force activity to hidden.
        $toform->visible = 0;

        $result = parent::set_data($toform);
        return $result;
    }

    /**
     * Return the data that will be used upon saving.
     *
     * @return null|array
     */
    public function get_data() {
        
        $fromform = parent::get_data();
        if (!empty($fromform)) {
            // Force completion tracking to automatic.
            //$fromform->completion = COMPLETION_TRACKING_AUTOMATIC;
            // Force activity to hidden.
            $fromform->visible = 0;
            // Format, regulate email notification delay.
            // only delay
            if ($fromform->messagedelaymode == PLANNEDMAIL_DELAYMODE_DELAY) {
                if (isset($fromform->emailperiod) && isset($fromform->emailperiodcount)) {
                    $fromform->maildelay = $fromform->emailperiod * $fromform->emailperiodcount;
                }
                $fromform->mailtime = null;
            } elseif($fromform->messagedelaymode == PLANNEDMAIL_DELAYMODE_DELAYANDTIME) {
                if (isset($fromform->mailtimedelay) && isset($fromform->mailtimecount)
                && isset($fromform->hours) && isset($fromform->minutes)) {
                    $fromform->maildelay = $fromform->mailtimedelay * $fromform->mailtimecount;
                    $fromform->mailtime = $fromform->hours * 3600 + $fromform->minutes*60;
                }
            }

            // delay + time
            if (empty($fromform->maildelay)) {
                $fromform->maildelay = 300;
            }
            if ($fromform->maildelay < 0) {
                $fromform->maildelay = 0;
            }
            unset($fromform->emailperiod);
            unset($fromform->emailperiodcount);
            unset($fromform->mailtimedelay);
            unset($fromform->mailtimecount);
            unset($fromform->hours);
            unset($fromform->minutes);
            unset($fromform->messagedelaymode);

            if (!empty($fromform->completionunlocked)) { 
                if (empty($fromform->completionmailsent)) {
                    $fromform->completionmailsent = false;
                }
                if (empty($fromform->completiondelayover)) {
                    $fromform->completiondelayover = false;
                }
            }
            
            
            // Some special handling for the wysiwyg editor field.
            $fromform->mailcontentformat = $fromform->mailcontent['format'];
            $fromform->mailcontent = $fromform->mailcontent['text'];
            $fromform->mailtoissuercontentformat = $fromform->mailtoissuercontent['format'];
            $fromform->mailtoissuercontent = $fromform->mailtoissuercontent['text'];

            // use plannedmail name as subject of mail
            $fromform->mailsubject = $fromform->name;

            
        }
        return $fromform;
    }
    /**
     *  Add custom completion rules for plannedmail.
     *
     * @return array Array of string IDs of added items, empty array if none
     */
    public function add_completion_rules() {
        $mform =& $this->_form;

        $group = array(
            $mform->createElement('checkbox', 'completionmailsent', '', get_string('completionmailsent', 'plannedmail')),
        );
        $mform->addGroup($group, 'completionmailsentgroup', get_string('completionmailsentgroup','plannedmail'), array(' '), false);

        $group = array(
            $mform->createElement('checkbox', 'completiondelayover', '', get_string('completiondelayover', 'plannedmail')),
        );
        $mform->addGroup($group, 'completiondelayovergroup', get_string('completiondelayovergroup','plannedmail'), array(' '), false);

        return array('completionmailsentgroup', 'completiondelayovergroup');
    }

    /**
     * A custom completion rule is enabled by plannedmail.
     *
     * @param array $data Input data (not yet validated)
     * @return bool True if one or more rules is enabled, false if none are;
     *   default returns false
     */
    public function completion_rule_enabled($data) {
        return (!empty($data['completionmailsent']) || !empty($data['completiondelayover']));
    }

    /**
     * Perform validation on the settings form
     * @param array $data
     * @param array $files
     */
    public function validation($data, $files) {

        $errors = parent::validation($data, $files);
        if (!empty($data['emailperiod'])) {
            $duration = $data['emailperiod'] * $data['emailperiodcount'];
        }

        if ($data['emailperiod'] * $data['emailperiodcount'] < 300) {
            $errors['maildelay'] = get_string('periodtoolow', 'plannedmail');
        }

        if ($data['completion'] == COMPLETION_TRACKING_MANUAL) {
            $errors['completion'] = get_string('nomanualcompletion', 'plannedmail');
        }

        return $errors;
    }

    private static function get_plannedmail_issuers_options() {
        global $COURSE;

        $possible_users =  get_enrolled_users(
            context_course::instance($COURSE->id),
            'mod/plannedmail:manageplannedmail',
            0,
            'u.id, u.firstname, u.lastname',
            'u.firstname, u.lastname',
            0,
            0,
            true
        );

        $issuers_options = array();
        // add noreply user first
        $noreply_user = core_user::get_noreply_user();
        $issuers_options[$noreply_user->id] = $noreply_user->username;
        foreach($possible_users as $possible_user) {
            $issuers_options[$possible_user->id] = $possible_user->firstname.' '.$possible_user->lastname;
        }
        return $issuers_options;
    }

    private function add_time_selector($hoursfieldname, $minutesfieldname, $groupname, $grouplabel) {
        $mform =& $this->_form;

        for ($i = 0; $i <= 23; $i++) {
            $hours[$i] =  sprintf("%02dh", $i) ;
          }
         for ($i = 0; $i < 60; $i++) {
            $minutes[$i] ="   " .  sprintf("%02d", $i);
         }
        
          $timearray=array();
          $timearray[]=& $mform->createElement('select', $hoursfieldname, '', $hours);
          $timearray[]=& $mform->createElement('select', $minutesfieldname, '', $minutes);
          return $mform->createElement('group', $groupname, $grouplabel, $timearray, '', false);
    }

}
