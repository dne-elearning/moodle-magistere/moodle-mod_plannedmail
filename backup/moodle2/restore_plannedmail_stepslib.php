<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Define all the restore steps that will be used by the restore_plannedmail_activity_task
 *
 * @package    mod_plannedmail
 * @copyright  2023 DNE - Ministere de l'Education Nationale
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

/**
 * Structure step to restore one plannedmail activity
 *
 * @package    mod_plannedmail
 * @copyright  2023 DNE - Ministere de l'Education Nationale
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class restore_plannedmail_activity_structure_step extends restore_activity_structure_step {
    /**
     * Define the structure for the plannedmail activity
     * @return void
     */
    protected function define_structure() {

        $paths = array();

        $paths[] = new restore_path_element('plannedmail', '/activity/plannedmail');

        // Return the paths wrapped into standard activity structure.
        return $this->prepare_activity_structure($paths);
    }

    /**
     * Process a plannedmail restore.
     *
     * @param object $data The data in object form
     * @return void
     */
    protected function process_plannedmail($data) {
        global $DB;

        $data = (object)$data;
        $data->course = $this->get_courseid();

        $data->timecreated = $this->apply_date_offset($data->timecreated);
        $data->timemodified = $this->apply_date_offset($data->timemodified);

        $noreplyid = \core_user::get_noreply_user()->id;
        $userinfo = $this->get_setting_value('userinfo');
        if ($userinfo) {
            $data->issuerid = $this->get_mappingid('user', $data->issuerid, $noreplyid);        
        } else {
            $data->issuerid = $noreplyid;
        }
        
        if ($data->issuerid == $noreplyid) {
            $data->sendmailtoissuer = false;
        }

        // Insert the plannedmail record.
        $newitemid = $DB->insert_record('plannedmail', $data);
        // Immediately after inserting "activity" record, call this.
        $this->apply_activity_instance($newitemid);
    }

}
