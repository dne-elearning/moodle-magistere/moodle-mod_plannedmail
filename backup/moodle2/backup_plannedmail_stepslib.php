<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Define all the backup steps that will be used by the backup_plannedmail_activity_task
 *
 * @package    mod_plannedmail
 * @copyright  2023 DNE - Ministere de l'Education Nationale
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

/**
 * Define the complete plannedmail structure for backup, with file and id annotations
 *
 * @package    mod_plannedmail
 * @copyright  2023 DNE - Ministere de l'Education Nationale
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class backup_plannedmail_activity_structure_step extends backup_activity_structure_step {

    /**
     * Define the structure for the assign activity
     * @return void
     */
    protected function define_structure() {

        // Define each element separated.
        $plannedmail = new backup_nested_element('plannedmail', array('id'), array(
            'name', 'timecreated', 'timemodified',
            'issuerid', 'mailcontent', 'mailcontentformat', 'mailsubject',
            'sendmailtoissuer', 'mailtoissuersubject', 'mailtoissuercontent', 'mailtoissuersubjectformat',
            'maildelay', 'mailtime', 'completionmailsent', 'completiondelayover' ,'suppresstarget'));

        // Define sources.
        $plannedmail->set_source_table('plannedmail', array('id' => backup::VAR_ACTIVITYID));

        // Return the root element (plannedmail), wrapped into standard activity structure.
        return $this->prepare_activity_structure($plannedmail);
    }
}
