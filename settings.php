<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Provides some custom settings for the mod_plannedmail module.
 *
 * @package     mod_plannedmail
 * @copyright   2023 DNE - Ministere de l'Education Nationale
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

if ($ADMIN->fulltree) {

    $settings->add(new admin_setting_heading('plannedmail_config',
        '<strong>'.get_string('pluginconfig', 'plannedmail').'</strong>', '')
    );

    $settings->add(new admin_setting_configselect('plannedmail/mailernoreply', get_string('mailernoreply', 'plannedmail'),
        get_string('mailernoreplysetting', 'plannedmail'), mod_plannedmail\mailer\plannedmail_mailer_manager::MAIL_OPTION_NOTIFICATION, mod_plannedmail\mailer\plannedmail_mailer_manager::get_options_noreply())
    );

    $settings->add(new admin_setting_configselect('plannedmail/mailerissuer', get_string('mailerissuer', 'plannedmail'),
        get_string('mailerissuersetting', 'plannedmail'), mod_plannedmail\mailer\plannedmail_mailer_manager::MAIL_OPTION_NOTIFICATION, mod_plannedmail\mailer\plannedmail_mailer_manager::get_options())
    );
}
