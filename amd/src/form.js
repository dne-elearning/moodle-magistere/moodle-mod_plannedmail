// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Javascript to handle plannedmail form.
 *
 * @package    mod_plannedmail
 * @copyright  2023 DNE - Ministere de l'Education Nationale
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
define(['jquery'], function($) {

    function init() {
        init_emailtoissuercontent();
    }

    function init_emailtoissuercontent() {
        toggle_emailtoissuercontent();

        $('#id_sendmailtoissuer').change(toggle_emailtoissuercontent);
        $('#id_mailsender').change(toggle_emailtoissuercontent);
    }

    function toggle_emailtoissuercontent() {
        if ($("#id_mailsender").val() == -10 || !$('#id_sendmailtoissuer').is(':checked')) {
            $('#fitem_id_mailtoissuercontent').hide();
        } else {
            $('#fitem_id_mailtoissuercontent').show();
        }
    }

	return {
		init: function() {
			init();
		}
	};

});