<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
/**
 * Adhoc task to send notification.
 *
 * @package    mod_plannedmail
 * @copyright  2023 DNE - Ministere de l'Education Nationale 
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace mod_plannedmail\task;

defined('MOODLE_INTERNAL') || die();


class send_notification extends \core\task\adhoc_task {

    private $cm;
    private $course;
    private $plannedmail;
    private $plannedmail_inprogress;

    public function execute() {
        global $DB;
       
        // we need to check that the criteria are still ok
        // check using user and activity id
        // if it's okay send the notification
        $data = $this->get_custom_data();
        

        if(!$DB->get_record('user', array('id' => $data->userid))){
            mtrace('User '.$data->userid.' doesn\'t exist anymore, aborting...');
            return;
        }

        $this->course = get_course($data->courseid);
        if(!$this->course) {
            mtrace('Course '.$data->courseid.' doesn\'t exist anymore, aborting...');
            return;
        }

        $modinfo = get_fast_modinfo($data->courseid, $data->userid);
        try {
            $this->cm = $modinfo->get_cm($data->cmid);
        } catch (\Exception $e) {
            mtrace('Erreur de récuparation du module de parcours');
            mtrace($e->getMessage());
            return;
        }
        
        $this->plannedmail = $DB->get_record('plannedmail', array('id' => $this->cm->instance));
        if(!$this->plannedmail) {
            mtrace('Plannedmail '.$this->cm->instance.' doesn\'t exist anymore, aborting...');
            return;
        }
        
        if (!has_capability('mod/plannedmail:receivemail', \context_module::instance($data->cmid), $data->userid)) {
            mtrace('User '.$data->userid.' doesn\'t have the capability to receive email anymore. Aborting.');
            return;
        }

        $this->plannedmail_inprogress = $DB->get_record('plannedmail_inprogress', 
            array('plannedmail' => $this->cm->instance, 'userid' =>$data->userid), '*', MUST_EXIST);
        if(!$this->plannedmail_inprogress) {
            mtrace('plannedmail_inprogress doesn\'t exist anymore, aborting...');
            return;
        }

        $ainfomod = new \core_availability\info_module($this->cm);
        $information = '';
        
        if ($ainfomod->is_available($information, false, $data->userid, $modinfo)) {

            $skipnotification = false;

            if (!empty($this->plannedmail->suppresstarget)) {
                $targetcomplete = plannedmail_check_target_completion($data->userid, $this->plannedmail->suppresstarget);
                if ($targetcomplete) {
                    mtrace('Plannedmail modules: User:'.$data->userid.
                              ' has completed target activity:'.$this->plannedmail->suppresstarget.' suppressing email.');
                    $skipnotification =  true;
                    $this->abort_sending();
                }
            }

            if (!$skipnotification) {
                mtrace('User '.$data->userid.' can receive the notification, access restriction is still valid.');
                $this->send_notification();
                if ($this->plannedmail->issuerid > 0 && $this->plannedmail->sendmailtoissuer) {
                    $this->send_notification_to_issuer();
                } 
            }
            

        } else {
            mtrace('User '.$data->userid.' cannot receive the notification, access restriction is not valid anymore.');
            $this->abort_sending();
        }
        mtrace('Task done...');
    }

    private function abort_sending() {
        global $DB;

        mtrace('The delay is over, but the notification could not be sent : this planned mail is marked as completed but not sent for this user.');
        $this->plannedmail_inprogress->completiontime = time();
        $this->plannedmail_inprogress->emailtime = 0;
        $this->plannedmail_inprogress->state = PLANNEDMAIL_STATE_COMPLETED_NOT_SENT;

        $DB->update_record('plannedmail_inprogress', $this->plannedmail_inprogress);

        // update completion info
        $completion = new \completion_info($this->course);
        if ($completion->is_enabled($this->cm) && $this->plannedmail->completiondelayover) {
            $completion->update_state($this->cm, COMPLETION_COMPLETE, $this->plannedmail_inprogress->userid);
        }

    }

    private function send_notification() {
        global $DB;

        $this->plannedmail_inprogress->completiontime = time();

        $userto = \core_user::get_user($this->plannedmail_inprogress->userid, '*', MUST_EXIST);
        $userfrom = \core_user::get_user($this->plannedmail->issuerid, '*', MUST_EXIST); // this function manage noreply too, nothing to do for this case


        $templateddetails = plannedmail_template_variables($this->plannedmail, $this->plannedmail_inprogress, $this->course, $userto);
        $plaintext = html_to_text($templateddetails['mailcontent'], 0);

        mtrace('Sending notification to user '.$userto->id.' for plannedmail '.$this->plannedmail->id);
        $mailer = \mod_plannedmail\mailer\plannedmail_mailer_manager::get_mailer($this->plannedmail);
        $result = $mailer->send_mail(
            $userto,
            $userfrom,
            $templateddetails['mailsubject'],
            $plaintext,
            $templateddetails['mailcontent'],
            $this->plannedmail
        );

        $this->plannedmail_inprogress->mailtime = time();
        if (!$result) {
            mtrace('Failed to send user '.$userto->id.' notification for plannedmail '.$this->plannedmail->id);
            $this->plannedmail_inprogress->state = PLANNEDMAIL_STATE_COMPLETED_AND_SENT_ERROR;
        } else {
            mtrace('Notification sent to user '.$userto->id.' for plannedmail '.$this->plannedmail->id);
            $this->plannedmail_inprogress->state = PLANNEDMAIL_STATE_COMPLETED_AND_SENT;
        }
        
        $transaction = $DB->start_delegated_transaction();
        $DB->update_record('plannedmail_inprogress', $this->plannedmail_inprogress);

        // update completion info
        $completion = new \completion_info($this->course);
        if ($completion->is_enabled($this->cm) && (
            ($result && $this->plannedmail->completionmailsent) || $this->plannedmail->completiondelayover)
            ) {
            $completion->update_state($this->cm, COMPLETION_COMPLETE, $this->plannedmail_inprogress->userid);
        }

        $transaction->allow_commit(); 
    }

    // send a notification to warn the issuer
    private function send_notification_to_issuer() {
        global $DB;

        $userto = \core_user::get_user($this->plannedmail->issuerid, '*', MUST_EXIST);
        $userfrom = \core_user::get_noreply_user(); 

        $templateddetails = plannedmail_template_variables($this->plannedmail, $this->plannedmail_inprogress, $this->course, $userto);
        $plaintext = html_to_text($templateddetails['mailtoissuercontent'], 0);

        mtrace('Sending notification to user (issuer) '.$userto->id.' for plannedmail '.$this->plannedmail->id);
        $mailer = \mod_plannedmail\mailer\plannedmail_mailer_manager::get_noreply_mailer();
        $result = $mailer->send_mail(
            $userto,
            $userfrom,
            $templateddetails['mailtoissuersubject'],
            $plaintext,
            $templateddetails['mailtoissuercontent'],
            $this->plannedmail
        );

        $this->plannedmail_inprogress->mailtime = time();
        if (!$result) {
            mtrace('Failed to send issuer '.$userto->id.'  notification for plannedmail '.$this->plannedmail->id);
        } else {
            mtrace('Issuer notification sent to user '.$userto->id.' for plannedmail '.$this->plannedmail->id);
        }
    }

}








