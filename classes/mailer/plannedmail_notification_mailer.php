<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Definition of plannedmail_notification_mailer class.
 *
 * @package    mod_plannedmail
 * @copyright  2023 DNE - Ministere de l'Education Nationale 
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

 namespace mod_plannedmail\mailer;

defined('MOODLE_INTERNAL') || die();

class plannedmail_notification_mailer extends \mod_plannedmail\mailer\plannedmail_mailer {

    public static function is_available() {
        return true; // notification are always available
    }

    public function send_mail($userto, $userfrom, $subject, $messageplain, $messagehtml, $plannedmail) {
        mtrace('Using notification mailer to send notification');
        $eventdata = new \core\message\message();
        $eventdata->courseid = $plannedmail->course;
        $eventdata->modulename = 'plannedmail';
        $eventdata->userfrom = $userfrom;
        $eventdata->userto = $userto;
        $eventdata->subject = $subject;
        $eventdata->fullmessage = $messageplain;
        $eventdata->fullmessageformat = FORMAT_HTML;
        $eventdata->fullmessagehtml = $messagehtml;
        $eventdata->smallmessage = $subject;

        // Required for messaging framework
        $eventdata->name = 'mod_plannedmail';
        $eventdata->component = 'mod_plannedmail';

        return message_send($eventdata);
    }
}