<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Definition of plannedmail_mailer_manager class, this class is reponsible for picking the good mailer
 *
 * @package    mod_plannedmail
 * @copyright  2023 DNE - Ministere de l'Education Nationale 
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

 namespace mod_plannedmail\mailer;

defined('MOODLE_INTERNAL') || die();

class plannedmail_mailer_manager {
    const MAIL_OPTION_NOTIFICATION = 1;
    const MAIL_OPTION_MESSAGE = 2;


    private static function create_notification_mailer() {
        return new \mod_plannedmail\mailer\plannedmail_notification_mailer();
    }

    private static function create_message_mailer() {
        return new \mod_plannedmail\mailer\plannedmail_message_mailer();
    }

    // pick the right mailer according to the global and plannedmail settings
    public static function get_mailer($plannedmail) {
        $conf = null;
        if ($plannedmail->issuerid == PLANNEDMAIL_NOREPLY_USER) {
            $conf = get_config('plannedmail', 'mailernoreply');
        } else {
            $conf = get_config('plannedmail', 'mailerissuer');
        }

        if ($conf == self::MAIL_OPTION_NOTIFICATION) {
            return self::create_notification_mailer();
        } elseif ($conf == self::MAIL_OPTION_MESSAGE) {
            return self::create_message_mailer();
        } else {
            return self::create_notification_mailer();
        }
    }

    public static function get_noreply_mailer() {
        return self::create_notification_mailer();
    }   

    public static function get_options() {
        $options = array();
        $options[self::MAIL_OPTION_NOTIFICATION] = get_string('optionnotification','plannedmail');

        if (\mod_plannedmail\mailer\plannedmail_message_mailer::is_available()) {
            $options[self::MAIL_OPTION_MESSAGE] = get_string('optionmessage','plannedmail');
        }

        return $options;
    }

    public static function get_options_noreply() {
        $options = array();
        $options[self::MAIL_OPTION_NOTIFICATION] = get_string('optionnotification','plannedmail');
        return $options;
    }
}