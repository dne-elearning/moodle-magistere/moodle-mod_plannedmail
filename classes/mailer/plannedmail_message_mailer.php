<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Definition of plannedmail_notification_mailer class.
 *
 * @package    mod_plannedmail
 * @copyright  2023 DNE - Ministere de l'Education Nationale 
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

 namespace mod_plannedmail\mailer;

defined('MOODLE_INTERNAL') || die();

class plannedmail_message_mailer extends \mod_plannedmail\mailer\plannedmail_mailer {

    public static function is_available() {
        $localplugins = \core_plugin_manager::instance()->get_plugins_of_type('local');
        if(isset($localplugins['mail']) && $localplugins['mail']->versiondisk >= 2017121405) {
            return true; 
        }
        return false;
    }

    public function send_mail($userto, $userfrom, $subject, $messageplain, $messagehtml, $plannedmail) {
        global $CFG;
        require_once ($CFG->dirroot.'/local/mail/message.class.php');
        require_once ($CFG->dirroot.'/local/mail/locallib.php');

        mtrace('Using message mailer (local mail) to send notification');
        $message = \local_mail_message::create($userfrom->id, $plannedmail->course);
        $message->save($subject, $messagehtml, FORMAT_HTML, 0);
        $message->add_recipient('to', $userto->id);
        $message->send();
        local_mail_send_notifications($message);
        return true;
    }
}