<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for plannedmail.
 *
 * @package    mod_plannedmail
 * @copyright  2023 DNE - Ministere de l'Education Nationale
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['pluginname'] = 'Plannedmail';
$string['plannedmail'] = 'plannedmail';
$string['pluginadministration'] = '';
$string['modulename'] = 'Plannedmail';
$string['modulenameplural'] = 'Plannedmails';
$string['modulename_help'] = 'The plannedmail activity provides a way for you to conditionnaly send notifications to students.';
// Alphabetized.
$string['activitycompleted'] = 'This activity has been marked as complete';
$string['completion'] = 'Completion';
$string['completionwillturnon'] = 'Note that adding this activity to the course will enable activity completion.';
$string['completiontime'] = 'Completion time';
$string['completiondatesupdated'] = 'Completion dates updated.';
$string['crontask'] = 'plannedmail cron task';
$string['cronwarning'] = 'The plannedmail scheduled task has not been run in the past 8 hours - Cron must be configured to allow plannedmail to function correctly.';
$string['days'] = 'Days';
$string['duration'] = 'Duration';
$string['duration_help'] = '<p>The plannedmail duration is the period of time between a user starting a plannedmail, and being marked as finished.
The plannedmail duration is specified as a period length (eg Weeks) and number of period (eg 7).</p>

<p>This example would mean that a user starting a plannedmail period now would be marked as compete in 7 weeks time.</p>
';
$string['mailcontent'] = 'Notification content (User)';
$string['mailcontent_help'] = 'When the module notifies a user, it takes the notification content from this field.';
$string['mailcontentmanager'] = 'Notification content (Manager)';
$string['mailcontentmanager_help'] = 'When the module notifies a user\'s manager(s), it takes the notification content from this field.';
$string['mailcontentdefaultvalue'] = 'This is a reminder notification from course %courseshortname%.';
$string['mailcontentmanagerdefaultvalue'] = 'This is a reminder notification from course %courseshortname%, regarding user %userfirstname% %userlastname%.';
$string['maildelay'] = 'Notification delay';
$string['maildelay_help'] = 'This setting controls how long the delay is after the completion of the acces condition.';
$string['emailrecipient'] = 'Notify recipient(s)';
$string['emailrecipient_help'] = 'When a notification needs to be sent out to prompt a user\'s re-engagement with the course, this setting controls if a notification is sent to the user, their manager(s), or both.';
$string['mailsubject'] = 'Notification subject (User)';
$string['mailsubject_help'] = 'When the module notifies a user, it takes the notification subject from this field.';
$string['mailsubjectmanager'] = 'Notification subject (Manager(s))';
$string['mailsubjectmanager_help'] = 'When the module notifies a user\'s manager(s), it takes the notification subject from this field.';
$string['mailtime'] = 'Notify time';
$string['errornoid'] = 'You must specify a course_module ID or an instance ID';
$string['errorplannedmailnotvalid'] = 'This plannedmail module is not enabled for your account.
Please contact your administrator if you feel this is in error';
$string['frequencytoohigh'] = 'The maximum reminder count with the delay period you have set is {$a}.';
$string['periodtoolow'] = 'The delay is too low - it must be at least 5 minutes.';
$string['hours'] = 'Hours';
$string['introdefaultvalue'] = 'This is a plannedmail activity.  Its purpose is to enforce a time lapse between the activities which preceed it, and the activities which follow it.';
$string['messageprovider:mod_plannedmail'] = 'Plannedmail notifications';
$string['minutes'] = 'Minutes';
$string['mustenablecompletionavailability'] = 'Completion tracking and restricted access settings must be enabled to use the plannedmail activity.';
$string['never'] = 'Never';
$string['newcompletiontime'] = 'New completion time';
$string['nochange'] = 'No change';
$string['nochangenoaccess'] = 'No change (user has not accessed course)';
$string['noemailattimex'] = 'Message scheduled for {$a} will not be sent because you have completed the target activity';
$string['nosuppresstarget'] = 'No target activity selected';
$string['oncompletion'] = 'On plannedmail completion';
$string['plannedmail:addinstance'] = 'Add a planned mail instance';
$string['plannedmail:receivemail'] = 'Receive mail';
$string['plannedmailduration'] = 'plannedmail duration';
$string['plannedmailfieldset'] = 'plannedmail details';
$string['plannedmailintro'] = 'plannedmail intro';
$string['plannedmailname'] = 'Plannedmail name';
$string['plannedmailsinprogress'] = 'plannedmails in progress';
$string['receiveemailattimex'] = 'Message will be sent on {$a}.';
$string['receiveemailattimexunless'] = 'Message will be sent on {$a} unless you complete target activity.';
$string['remindercount'] = 'Reminder count';
$string['remindercount_help'] = 'This is the number of times an e-mail is sent after each delay period. There are some limits to the values you can use<ul>
<li>less than 24 hrs - limit of 2 reminders.</li>
<li>less than 5 days - limit of 10 reminders.</li>
<li>less than 15 days - limit of 26 reminders.</li>
<li>over 15 days - maximum limit of 40 reminders.</li></ul>';
$string['resetbyfirstaccess'] = 'By first course access and a duration of: {$a}';
$string['resetbyenrolment'] = 'By enrolment creation date and a duration of: {$a}';
$string['resetbyspecificdate'] = 'By specified date';
$string['resetcompletion'] = 'Reset completion date';
$string['search:activity'] = 'plannedmail - activity information';
$string['specifydate'] = 'Set completion date to:';
$string['suppressemail'] = 'Suppress notification if target activity complete';
$string['suppressemail_help'] = 'This option instructs the activity to suppress notifications to users where a named activity is complete.';
$string['suppresstarget'] = 'Target activity.';
$string['suppresstarget_help'] = 'Use this dropdown to choose which activity should be checked for completion before sending the reminder notification.';
$string['userandmanager'] = 'User and Manager(s)';
$string['weeks'] = 'Weeks';
$string['withselectedusers'] = 'With selected users...';
$string['withselectedusers_help'] = '* Send message - For sending a message to one or more participants
* Reset completion date by course access - For adjusting the plannedmail completion date based on the first access to this course.';
$string['seconds'] = 'Seconds';


// plannedmail specific strings
$string['privacy:metadata:plannedmail'] = 'plannedmail ID';
$string['privacy:metadata:userid'] = 'User id this record relates to';
$string['privacy:metadata:completiontime'] = 'When this module was completed';
$string['privacy:metadata:mailtime'] = 'When this user was mailed';
$string['privacy:metadata:emailsent'] = 'Email has been sent';
$string['privacy:metadata:plannedmail_inprogress'] = 'plannedmail activities in progress';
$string['mailissuer'] = 'Mail issuer';
$string['plannedmail:manageplannedmail'] = 'Manage planned mails';
$string['sendmailtoissuer'] = 'Send mail to issuer';
$string['mailtoissuercontent'] = 'Notification content (issuer)';
$string['mailtoissuercontent_help'] = 'When the module notifies the issuer, it takes the notification content from this field.';
$string['mailtoissuercontentdefaultvalue'] = 'This is a reminder notification from course %courseshortname%, regarding user %userfirstname% %userlastname%.';
$string['mailtoissuersubject'] = 'Notification subject (issuer)';
$string['mailtoissuersubject_help'] = 'When the module notifies the issuer, it takes the notification subject from this field';
$string['maildelayandtime'] = 'Delay with precise time.';
$string['delay'] = 'Delay';
$string['delayandprecisetime'] = 'Delay with precise time';
$string['messagedelay_mode'] = 'Message delay mode ';
$string['mailtimedelay'] = 'Notification delay and time';
$string['In'] = 'In';
$string['at'] = 'at';
$string['check_availability_conditions_task'] = 'Schedule task to check availability condition of plannedmail activity modules';
$string['pluginconfig'] = 'Plannedmail configuration';
$string['mailernoreply'] = 'Mailer used with the noreply send';
$string['mailernoreplysetting'] = 'The mailer to be used when sending message with the noreply user';
$string['mailerissuer'] = 'Mailer used with a selected issuer';
$string['mailerissuersetting'] = 'The mailer to be used when sending message with a selected issuer';
$string['optionnotification'] = 'Notification';
$string['optionmessage'] = 'Message';
$string['completionmailsent'] = 'The mail must have been sent.';
$string['completionmailsentgroup'] = 'Mail sent';
$string['completiondelayover'] = 'The delay must be over (mail doesn\'t need to be sent).';
$string['completiondelayovergroup'] = 'Delay is over';
$string['nomanualcompletion'] = 'Manual completion isn\'t availaible for plannedmail.';
$string['state_notstarted'] = 'Not started';
$string['state_waiting'] = 'Waiting';
$string['state_completed_and_sent'] = 'Completed and mail sent';
$string['state_completed_not_sent'] = 'Completed but mail not sent';
$string['state_completed_and_sent_error'] = 'Completed but with a mail sent error';
$string['pmstate'] = 'Plannedmail state';

$string['maildelay_weeks'] = '{$a} weeks';
$string['maildelay_week'] = '{$a} week';
$string['maildelay_days'] = '{$a} days';
$string['maildelay_day'] = '{$a} day';
$string['maildelay_hours'] = '{$a} hours';
$string['maildelay_hour'] = '{$a} hour';
$string['maildelay_minutes'] = '{$a} minutes';
$string['maildelay_minute'] = '{$a} minute';


$string['emiterdelay'] = 'Emiter : {$a->issuer} - delay : {$a->delay}';
$string['emiterdelay_hour'] = 'Emiter : {$a->issuer} - delay : {$a->delay} at {$a->hour}';
$string['emiter_notification'] = 'notification';
$string['emiter_unknown'] = 'unknown';
