<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for plannedmail.
 *
 * @package    mod_plannedmail
 * @copyright  2023 DNE - Ministere de l'Education Nationale 
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['pluginname'] = 'Message planifié';
$string['modulename'] = 'Message planifié';
$string['modulenameplural'] = 'Messages planifiés';
$string['modulename_help'] = 'L\'activité message planifié permet d\'envoyer des messages aux étudiants.';
$string['plannedmailfieldset'] = 'Caractéristiques du message';
$string['mailcontent'] = 'Corps du message';
$string['mailcontent_help'] = 'Ce texte sera envoyé quand le module notifiera un utilisateur.';
$string['mailsubject'] = 'Objet du message';
$string['mailsubject_help'] = 'L\'objet du message qui sera envoyé quand le module notifiera un utilisateur.';
$string['thirdpartyemails'] = 'Destinatires externes';
$string['thirdpartyemails_help'] = 'Une liste d\'adresses emails, séparées par des virgules, qui devraient recevoir un mail quand l\'utilisateur en reçoit un.';
$string['maildelay'] = 'Délai d\'envoi du message';
$string['maildelay_help'] = 'Délai avant l\'envoi du message. Ce délai s\'active un fois que les conditions d\'accès sont remplies.';
$string['plannedmailname'] = 'Nom du message planifié';
$string['mailissuer'] = 'Emetteur du message';
$string['plannedmail:manageplannedmail'] = 'Gérer les messages planifiés';
$string['plannedmail:receivemail'] = 'Recevoir les messages';
$string['plannedmail:addinstance'] = 'Ajouter une activité message planifié';
$string['sendmailtoissuer'] = 'Envoi d\'un message au formateur';
$string['mailtoissuercontent'] = 'Contenu du message au formateur';
$string['mailtoissuercontent_help'] = 'Le contenu du message qui sera envoyé au formateur quand le module notifiera un utilisateur.';
$string['mailtoissuercontentdefaultvalue'] = 'Ceci est une notification de rappel du cours %courseshortname%, portant sur l\'utilisateur %userfirstname% %userlastname%.';
$string['mailtoissuersubject'] = 'Sujet du message au formateur';
$string['mailtoissuersubject_help'] = 'Le contenu du message qui sera envoyé au formateur quand le module notifiera un utilisateur.';
$string['maildelayandtime'] = 'Délai avec heure précise';
$string['delay'] = 'Délai';
$string['delayandprecisetime'] = 'Délai avec heure précise';
$string['messagedelay_mode'] = 'Mode de délai d\'envoi du message';
$string['mailtimedelay'] = 'Délai et heure d\'envoi du message';
$string['In'] = 'Dans';
$string['at'] = 'à';
$string['check_availability_conditions_task'] = 'Tâche planifié de vérification des restrictions d\'accès aux activités message planifié';
$string['pluginconfig'] = 'Configuration de Message planifié';
$string['mailernoreply'] = 'Mode d\'envoi pour noreply.';
$string['mailernoreplysetting'] = 'Le mode d\'envoi du message qui sera utilisé si l\'émetteur noreply est selectionné.';
$string['mailerissuer'] = 'Mode d\'envoi pour émetteur précis';
$string['mailerissuersetting'] = 'Le mode d\'envoi du message qui sera utilisé si un émetteur précis est selectionné.';
$string['optionnotification'] = 'Notification';
$string['optionmessage'] = 'Message';
$string['completionmailsent'] = 'Le message a été émis';
$string['completionmailsentgroup'] = 'Message émis';
$string['completiondelayover'] = 'Le délai d\'envoi doit être dépassé (que le message ait été émis ou non)';
$string['completiondelayovergroup'] = 'Délai d\'envoi';
$string['nomanualcompletion'] = 'L\'achèvement d\'activité manuel n\'est pas disponible pour message planifié.';
$string['mailtime'] = 'Heure d\'envoi de la notification';
$string['completiontime'] = 'Heure de complétion de l\'activité';
$string['state_notstarted'] = 'Non concerné';
$string['state_waiting'] = 'En attente';
$string['state_completed_and_sent'] = 'Terminé avec message envoyé';
$string['state_completed_not_sent'] = 'Terminé mais message non envoyé';
$string['state_completed_and_sent_error'] = 'Terminé mais erreur à l\'envoi du message';
$string['pmstate'] = 'Etat du message planifié';
$string['cronwarning'] = 'La tâche de message planifié n\'a pas tournée depuis 8h. Celle-ci doit être configurée pour que message planifié se comporte correctment.';
$string['messageprovider:mod_plannedmail'] = 'Notifications de message planifié';
$string['days'] = 'Jours';
$string['weeks'] = 'Semaines';
$string['hours'] = 'heures';
$string['minutes'] = 'Minutes';
$string['secondes'] = 'Secondes';
$string['suppressemail'] = 'Ne pas envoyer la notification si l\'activité cible est complétée';
$string['suppressemail_help'] = 'Cette option permet de ne pas envoyer la notification à l\'utilisateur si celui-ci a complété l\'activité cible.';
$string['suppresstarget'] = 'Activité cible';
$string['suppresstarget_help'] = 'Ce sélecteur permet de choisir l\'activité cible. La complétion de cette activité bloquera l\'envoi de la notification .';
$string['nosuppresstarget'] = 'Aucune activité cible selectionnée';

$string['maildelay_weeks'] = '{$a} semaines';
$string['maildelay_week'] = '{$a} semaine';
$string['maildelay_days'] = '{$a} jours';
$string['maildelay_day'] = '{$a} jour';
$string['maildelay_hours'] = '{$a} heures';
$string['maildelay_hour'] = '{$a} heure';
$string['maildelay_minutes'] = '{$a} minutes';
$string['maildelay_minute'] = '{$a} minute';

$string['emiterdelay'] = 'Émetteur : {$a->issuer} - Délai : {$a->delay}';
$string['emiterdelay_hour'] = 'Émetteur : {$a->issuer} - Délai : {$a->delay} à {$a->hour}';
$string['emiter_notification'] = 'notification';
$string['emiter_unknown'] = 'inconnu';
