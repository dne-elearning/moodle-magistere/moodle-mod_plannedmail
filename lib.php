<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This page lists all the instances of plannedmail in a particular course
 *
 * @package    mod_plannedmail
 * @copyright  2023 DNE - Ministere de l'Education Nationale
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();
require_once($CFG->libdir."/completionlib.php");

define('PLANNEDMAIL_DELAYMODE_DELAY', 0);
define('PLANNEDMAIL_DELAYMODE_DELAYANDTIME', 1);
define('PLANNEDMAIL_NOREPLY_USER', -10);

define('PLANNEDMAIL_STATE_WAITING', 1);
define('PLANNEDMAIL_STATE_COMPLETED_AND_SENT', 2);
define('PLANNEDMAIL_STATE_COMPLETED_NOT_SENT', 3);
define('PLANNEDMAIL_STATE_COMPLETED_AND_SENT_ERROR', 4); // the mail should have been sent but there was an error

/**
 * Given an object containing all the necessary data,
 * (defined by the form in mod_form.php) this function
 * will create a new instance and return the id number
 * of the new instance.
 *
 * @param object $plannedmail An object from the form in mod_form.php
 * @return int The id of the newly inserted plannedmail record
 */
function plannedmail_add_instance($plannedmail) {
    global $DB;

    $plannedmail->timecreated = time();
    $plannedmail->timemodified = time();

    if (empty($plannedmail->suppressemail)) {
        // User didn't tick the box indicating they wanted to suppress email if a certain activity was complete.
        // Force the 'target activity' field to be 0 (ie no target).
        $plannedmail->suppresstarget = 0;
    }
    unset($plannedmail->suppressemail);
    
    // Check course has completion enabled, and enable it if not, and user has permission to do so.
    $course = $DB->get_record('course', array('id' => $plannedmail->course));
    if (empty($course->enablecompletion)) {
        $coursecontext = context_course::instance($course->id);
        if (has_capability('moodle/course:update', $coursecontext)) {
            $data = array('id' => $course->id, 'enablecompletion' => '1');
            $DB->update_record('course', $data);
            rebuild_course_cache($course->id);
        }
    }

    return $DB->insert_record('plannedmail', $plannedmail);
}


/**
 * Given an object containing all the necessary data,
 * (defined by the form in mod_form.php) this function
 * will update an existing instance with new data.
 *
 * @param object $plannedmail An object from the form in mod_form.php
 * @return boolean Success/Fail
 */
function plannedmail_update_instance($plannedmail) {
    global $DB;
    
    $plannedmail->timemodified = time();
    $plannedmail->id = $plannedmail->instance;

    
    // If they didn't chose to suppress email, do nothing.
    if (!$plannedmail->suppressemail) {
        $plannedmail->suppresstarget = 0;// No target to be set.
    }
    unset($plannedmail->suppressemail);

    $result = $DB->update_record('plannedmail', $plannedmail);
    return $result;
}


/**
 * Given an ID of an instance of this module,
 * this function will permanently delete the instance
 * and any data that depends on it.
 *
 * @param int $id Id of the module instance
 * @return boolean Success/Failure
 */
function plannedmail_delete_instance($id) {
    global $DB;

    if (! $plannedmail = $DB->get_record('plannedmail', array('id' => $id))) {
        return false;
    }

    $result = true;

    // Delete any dependent records here.
    if (! $DB->delete_records('plannedmail_inprogress', array('plannedmail' => $plannedmail->id))) {
        $result = false;
    }

    if (! $DB->delete_records('plannedmail', array('id' => $plannedmail->id))) {
        $result = false;
    }

    return $result;
}

/**
 * Print the grade information for this user.
 *
 * @param stdClass $course
 * @param stdClass $user
 * @param stdClass $mod
 * @param stdClass $plannedmail
 */
function plannedmail_user_outline($course, $user, $mod, $plannedmail) {
    return;
}


/**
 * Prints the complete info about a user's interaction.
 *
 * @param stdClass $course
 * @param stdClass $user
 * @param stdClass $mod
 * @param stdClass $plannedmail
 */
function plannedmail_user_complete($course, $user, $mod, $plannedmail) {
    return true;
}

/**
 * Obtains the automatic completion state for this plannedmail based on any conditions
 * in plannedmail settings.
 *
 * @param object $course Course
 * @param object $cm Course-module
 * @param int $userid User ID
 * @param bool $type Type of comparison (or/and; can be used as return value if no conditions)
 * @return bool True if completed, false if not. (If no conditions, then return
 *   value depends on comparison type)
 */
function plannedmail_get_completion_state($course, $cm, $userid, $type) {
    global $DB;
    
    $plannedmail = $DB->get_record('plannedmail', array('id' => $cm->instance), '*', MUST_EXIST);
    $plannedmail_inprogress = $DB->get_record('plannedmail_inprogress', array(
        'plannedmail'=>$plannedmail->id, 'userid' => $userid
    ));

    if ($plannedmail->completiondelayover) {
        if (!$plannedmail_inprogress) { return false; }
        
        return ($plannedmail_inprogress->state == PLANNEDMAIL_STATE_COMPLETED_AND_SENT 
        || $plannedmail_inprogress->state == PLANNEDMAIL_STATE_COMPLETED_NOT_SENT || $plannedmail_inprogress->state == PLANNEDMAIL_STATE_COMPLETED_AND_SENT_ERROR); 
    } 
    else if ($plannedmail->completionmailsent) {
        if (!$plannedmail_inprogress) { return false; }

        return ($plannedmail_inprogress->state == PLANNEDMAIL_STATE_COMPLETED_AND_SENT 
            || $plannedmail_inprogress->state == PLANNEDMAIL_STATE_COMPLETED_AND_SENT_ERROR);
    }
    
    return $type;
}

/**
 * Add a get_coursemodule_info function in case any plannedmail type wants to add 'extra' information
 * for the course (see resource).
 *
 * Given a course_module object, this function returns any "extra" information that may be needed
 * when printing this activity in a course listing.  See get_array_of_activities() in course/lib.php.
 *
 * @param stdClass $coursemodule The coursemodule object (record).
 * @return cached_cm_info An object on information that the courses
 *                        will know about (most noticeably, an icon).
 */
function plannedmail_get_coursemodule_info($coursemodule) {
    global $DB;

    $dbparams = ['id' => $coursemodule->instance];
    $fields = 'id, name, completionmailsent, completiondelayover';
    if (!$plannedmail = $DB->get_record('plannedmail', $dbparams, $fields)) {
        return false;
    }

    $result = new cached_cm_info();
    $result->name = $plannedmail->name;

    // Populate the custom completion rules as key => value pairs, but only if the completion mode is 'automatic'.
    if ($coursemodule->completion == COMPLETION_TRACKING_AUTOMATIC) {
        $result->customdata['customcompletionrules']['completionmailsent'] = $plannedmail->completionmailsent;
        $result->customdata['customcompletionrules']['completiondelayover'] = $plannedmail->completiondelayover;
    }

    return $result;
}

/**
 * Callback which returns human-readable strings describing the active completion custom rules for the module instance.
 *
 * @param cm_info|stdClass $cm object with fields ->completion and ->customdata['customcompletionrules']
 * @return array $descriptions the array of descriptions for the custom rules.
 */
function mod_plannedmail_get_completion_active_rule_descriptions($cm) {
    // Values will be present in cm_info, and we assume these are up to date.
    if (empty($cm->customdata['customcompletionrules']) || $cm->completion != COMPLETION_TRACKING_AUTOMATIC) {
        return [];
    }

    $descriptions = [];
    foreach ($cm->customdata['customcompletionrules'] as $key => $val) {
        switch ($key) {
            case 'completionmailsent':
                if (!empty($val)) {
                    $descriptions[] = get_string('completionmailsent', 'plannedmail');
                }
                break;
            case 'completionreplies':
                if (!empty($val)) {
                    $descriptions[] = get_string('completiondelayover', 'plannedmail');
                }
                break;
            default:
                break;
        }
    }
    return $descriptions;
}

/**
 * Prints the recent activity.
 *
 * @param stdClass $course
 * @param stdClass $isteacher
 * @param stdClass $timestart
 */
function plannedmail_print_recent_activity($course, $isteacher, $timestart) {
    return false;  // True if anything was printed, otherwise false.
}

/**
 * Send plannedmail notifications using the messaging system.
 *
 * @param object $userto User we are sending the notification to
 * @param string $subject message subject
 * @param string $messageplain plain text message
 * @param string $messagehtml html message
 * @param object $plannedmail database record
 */
function plannedmail_send_notification($userto, $subject, $messageplain, $messagehtml, $plannedmail) {
    $eventdata = new \core\message\message();
    $eventdata->courseid = $plannedmail->courseid;
    $eventdata->modulename = 'plannedmail';
    $eventdata->userfrom = core_user::get_support_user();
    $eventdata->userto = $userto;
    $eventdata->subject = $subject;
    $eventdata->fullmessage = $messageplain;
    $eventdata->fullmessageformat = FORMAT_HTML;
    $eventdata->fullmessagehtml = $messagehtml;
    $eventdata->smallmessage = $subject;

    // Required for messaging framework
    $eventdata->name = 'mod_plannedmail';
    $eventdata->component = 'mod_plannedmail';

    return message_send($eventdata);
}


/**
 * Template variables into place in supplied email content.
 *
 * @param object $plannedmail db record of details for this activity
 * @param object $inprogress record of user participation in this activity - semiplanned future enhancement.
 * @param object $user record of user being reengaged.
 * @return array - the content of the fields after templating.
 */
function plannedmail_template_variables($plannedmail, $inprogress, $course, $user) {
    global $CFG, $DB;

    require_once($CFG->dirroot.'/user/profile/lib.php');

    $templatevars = array(
        '/%courseshortname%/' => $course->shortname,
        '/%coursefullname%/' => $course->fullname,
        '/%courseid%/' => $course->id,
        '/%userfirstname%/' => $user->firstname,
        '/%userlastname%/' => $user->lastname,
        '/%userid%/' => $user->id,
        '/%usercity%/' => $user->city,
        '/%userinstitution%/' => $user->institution,
        '/%userdepartment%/' => $user->department,
        '/%date%/' => date('d-m-Y'),
        '/%time%/' => date('H:i'),
    );
    // Add the users course groups as a template item.
    $groups = $DB->get_records_sql_menu("SELECT g.id, g.name
                                   FROM {groups_members} gm
                                   JOIN {groups} g
                                    ON g.id = gm.groupid
                                  WHERE gm.userid = ? AND g.courseid = ?
                                   ORDER BY name ASC", array($user->id, $course->id));

    if (!empty($groups)) {
        $templatevars['/%usergroups%/'] = implode(', ', $groups);
    } else {
        $templatevars['/%usergroups%/'] = '';
    }

    // Now do custom user fields;
    $fields = profile_get_custom_fields();
    if (!empty($fields)) {
        $userfielddata = $DB->get_records('user_info_data', array('userid' => $user->id), '', 'fieldid, data, dataformat');
        foreach ($fields as $field) {
            if (!empty($userfielddata[$field->id])) {
                if ($field->datatype == 'datetime') {
                    if (!empty($field->param3)) {
                        $format = get_string('strftimedaydatetime', 'langconfig');
                    } else {
                        $format = get_string('strftimedate', 'langconfig');
                    }

                    $templatevars['/%profilefield_'.$field->shortname.'%/'] = userdate($userfielddata[$field->id]->data, $format);
                } else {
                    $templatevars['/%profilefield_'.$field->shortname.'%/'] = format_text($userfielddata[$field->id]->data, $userfielddata[$field->id]->dataformat);
                }

            } else {
                $templatevars['/%profilefield_'.$field->shortname.'%/'] = '';
            }
        }
    }
    $patterns = array_keys($templatevars); // The placeholders which are to be replaced.
    $replacements = array_values($templatevars); // The values which are to be templated in for the placeholders.

    // Array to describe which fields in plannedmail object should have a template replacement.
    $replacementfields = array('mailsubject', 'mailcontent', 'mailtoissuersubject', 'mailtoissuercontent');

    $results = array();
    // Replace %variable% with relevant value everywhere it occurs in plannedmail->field.
    foreach ($replacementfields as $field) {
        $results[$field] = preg_replace($patterns, $replacements, $plannedmail->$field);
    }

    // Apply enabled filters to email content.
    $options = array(
            'context' => context_course::instance($course->id),
            'noclean' => true,
            'trusted' => true,
            'newlines' => false,
    );
    $contentfields = array('mailcontent', 'mailtoissuercontent');
    foreach ($contentfields as $field) {
        $results[$field] = format_text($results[$field], FORMAT_MOODLE, $options);
    }

    return $results;
}

/**
 * Must return an array of user records (all data) who are participants
 * for a given instance of plannedmail. Must include every user involved
 * in the instance, independient of his role (student, teacher, admin...)
 * See other modules as example.
 *
 * @param int $plannedmailid ID of an instance of this module
 * @return mixed boolean/array of students
 */
function plannedmail_get_participants($plannedmailid) {
    return false;
}


/**
 * Checks if a scale is being used.
 *
 * This is used by the backup code to decide whether to back up a scale
 * @param int $plannedmailid
 * @param int $scaleid
 * @return boolean True if the scale is used by the assignment
 */
function plannedmail_scale_used($plannedmailid, $scaleid) {
    $return = false;

    return $return;
}


/**
 * This is used to find out if scale used anywhere.
 *
 * @param int $scaleid
 * @return boolean True if the scale is used by any plannedmail
 */
function plannedmail_scale_used_anywhere($scaleid) {
    return false;
}


/**
 * Execute post-install custom actions for the module
 * This function was added in 1.9
 *
 * @return boolean true if success, false on error
 */
function plannedmail_install() {
    return true;
}


/**
 * Implementation of the function for printing the form elements that control
 * whether the course reset functionality affects the choice.
 *
 * @param object $mform form passed by reference
 */
function plannedmail_reset_course_form_definition(&$mform) {
    $mform->addElement('header', 'plannedmailheader', get_string('modulenameplural', 'plannedmail'));
}

/**
 * Course reset form defaults.
 *
 * @param object $course
 * @return array
 */
function plannedmail_reset_course_form_defaults($course) {
    return array('reset_plannedmail' => 1);
}

/**
 * Actual implementation of the reset course functionality, delete all the
 * choice responses for course $data->courseid.
 *
 * @param object $data the data submitted from the reset course.
 * @return array status array
 */
function plannedmail_reset_userdata($data) {
    global $DB;

    $componentstr = get_string('modulenameplural', 'plannedmail');
    $status = array();

    if (!empty($data->reset_plannedmail)) {
        $plannedmailsql = "SELECT ch.id
                       FROM {plannedmail} ch
                       WHERE ch.course=?";

        $DB->delete_records_select('plannedmail_inprogress', "plannedmail IN ($plannedmailsql)", array($data->courseid));
        $status[] = array('component' => $componentstr, 'item' => get_string('removeresponses', 'plannedmail'), 'error' => false);
    }

    return $status;
}

/**
 * Return the list of Moodle features this module supports
 *
 * @param string $feature FEATURE_xx constant for requested feature
 * @return mixed True if module supports feature, null if doesn't know
 */
function plannedmail_supports($feature) {
    switch($feature) {
        case FEATURE_GROUPS:
            return false;
        case FEATURE_GROUPINGS:
            return false;
        case FEATURE_MOD_INTRO:
            return false;
        case FEATURE_COMPLETION_TRACKS_VIEWS:
            return false;
        case FEATURE_COMPLETION_HAS_RULES:
            return true;
        case FEATURE_GRADE_HAS_GRADE:
            return false;
        case FEATURE_GRADE_OUTCOMES:
            return false;
        case FEATURE_BACKUP_MOODLE2:
            return true;
        case FEATURE_SHOW_DESCRIPTION:
            return true;

        default:
            return null;
    }
}

/**
 * Process an arbitary number of seconds, and prepare to display it as 'W seconds', 'X minutes', or Y hours or Z weeks.
 *
 * @param int $duration FEATURE_xx constant for requested feature
 * @param boolean $periodstring - return period as string.
 * @return array
 */
function plannedmail_get_readable_duration($duration, $periodstring = false) {
    $period = 1; // Default to dealing in seconds.
    $periodcount = $duration; // Default to dealing in seconds.
    $periods = array(WEEKSECS, DAYSECS, HOURSECS, MINSECS);
    foreach ($periods as $period) {
        if (($duration % $period) == 0) {
            // Duration divides exactly into periods.
            $periodcount = floor((int)$duration / (int)$period);
            break;
        }
    }
    if ($periodstring) {
        // Caller wants function to return in the format (30, 'minutes'), not (30, 60).
        if ($period == MINSECS) {
            $period = get_string('minutes', 'plannedmail');
        } else if ($period == HOURSECS) {
            $period = get_string('hours', 'plannedmail');
        } else if ($period == DAYSECS) {
            $period = get_string('days', 'plannedmail');
        } else if ($period == WEEKSECS) {
            $period = get_string('weeks', 'plannedmail');
        } else {
            $period = get_string('weeks', 'plannedmail');
        }
    }
    return array($periodcount, $period); // Example 5, 60 is 5 minutes.
}

/**
 * Check if user has completed the named course moduleid
 * @param int $userid idnumber of the user to be checked.
 * @param int $targetcmid the id of the coursemodule we should be checking.
 * @return bool true if user has completed the target activity, false otherwise.
 */
function plannedmail_check_target_completion($userid, $targetcmid) {
    global $DB;
    // This plannedmail is focused on getting people to do a particular (ie targeted) activity.
    // Behaviour of the module changes depending on whether the target activity is already complete.
    $conditions = array('userid' => $userid, 'coursemoduleid' => $targetcmid);
    $activitycompletion = $DB->get_record('course_modules_completion', $conditions);
    if ($activitycompletion) {
        // There is a target activity, and completion is enabled in that activity.
        $userstate = $activitycompletion->completionstate;
        if (in_array($userstate, array(COMPLETION_COMPLETE, COMPLETION_COMPLETE_PASS, COMPLETION_COMPLETE_FAIL))) {
            return true;
        }
    }
    return false;
}


/**
 * For each plannedmail, check if a user pass the access restrictions, if so create the notification task.
 *
 * @return void
 */
function check_availibility_condition() {
    global $DB;

    // we need to get all the plannedmail activites
    $sql = '
        SELECT 
        pm.id AS instanceid,
        pm.course AS courseid,
        cm.id AS cmid
        FROM {modules} m
        INNER JOIN {course_modules} cm ON m.id = cm.module
        INNER JOIN {plannedmail} pm ON cm.instance = pm.id
        WHERE m.name LIKE "plannedmail"';
    $plannedmailmodules = $DB->get_records_sql($sql);

    // now all the users for each activity
    foreach($plannedmailmodules as $pmm) {
        mtrace('Checking for plannedmail '.$pmm->instanceid.'...');

        $users =  get_enrolled_users(
            context_module::instance($pmm->cmid),
            'mod/plannedmail:receivemail',
            0,
            'u.id, u.firstname, u.lastname',
            'u.firstname, u.lastname',
            0,
            0,
            true
        );

        // we check each user
        foreach($users as $user) {
            if ($DB->record_exists('plannedmail_inprogress', 
                array('plannedmail' => $pmm->instanceid, 'userid' => $user->id))) {
                mtrace('plannedmail_inprogress record is already existing for user '.$user->id. ' and plannedmail '.$pmm->instanceid.', skipping...');
                continue;
            };


            $modinfo = get_fast_modinfo($pmm->courseid, $user->id);
            $cm = $modinfo->get_cm($pmm->cmid);
            $ainfomod = new \core_availability\info_module($cm);
            $information = '';

            if ($ainfomod->is_available($information, false, $user->id, $modinfo)) {
                mtrace('User '.$user->id.' can receive the notification, creating task...');

                $pm_inprogress = new stdClass();
                $pm_inprogress->plannedmail = $pmm->instanceid;
                $pm_inprogress->userid = $user->id;
                $pm_inprogress->state = PLANNEDMAIL_STATE_WAITING;

                $transac = $DB->start_delegated_transaction();
                create_notification_task($pmm->instanceid, $pmm->courseid, $pmm->cmid, $user->id);
                $DB->insert_record('plannedmail_inprogress', $pm_inprogress);
                $transac->allow_commit();
                mtrace('Notification task created for user '.$user->id);
                
            } else {
                mtrace('User '.$user->id.' cannot receive the notification.');
            }
        }
    }
}

/**
 * Create the notification task for a specific plannedmail instance and user
 * This function is called by the function check_availibility_condition
 *
 * @param int $pmid Plannedmail instance id
 * @param int $courseid Course id
 * @param int $cmid Course module id
 * @param int $userid idnumber of the user
 * @return void
 */
function create_notification_task($pmid, $courseid, $cmid, $userid) {
    global $DB;

    $notificationtask = new mod_plannedmail\task\send_notification();

    // add custom data
    $notificationtask->set_custom_data(array(
        'courseid' => $courseid,
        'cmid' => $cmid,
        'userid' => $userid
    ));

    $plannedmail = $DB->get_record('plannedmail', array('id' => $pmid));
    $runtime = compute_notification_time($plannedmail);
    
    $notificationtask->set_next_run_time($runtime);
    // queue it
    \core\task\manager::queue_adhoc_task($notificationtask);
}

function compute_notification_time($plannedmail) {
    // compute run time base based on maildelay and mailtime
    $runtime = null;
    if ($plannedmail->mailtime) {
        // delay + time mode 
        $beginningofday = strtotime("midnight", time()+$plannedmail->maildelay);
        $runtime = $beginningofday + $plannedmail->mailtime;
    } else {
        // only delay
        $runtime = time()+$plannedmail->maildelay;
    }
    return $runtime;
}

function get_state_string($status = null) {
    switch($status) {
        case PLANNEDMAIL_STATE_WAITING:
            return get_string('state_waiting','plannedmail');
        case PLANNEDMAIL_STATE_COMPLETED_AND_SENT:
            return get_string('state_completed_and_sent','plannedmail');
        case PLANNEDMAIL_STATE_COMPLETED_NOT_SENT:
            return get_string('state_completed_not_sent','plannedmail');
        case PLANNEDMAIL_STATE_COMPLETED_AND_SENT_ERROR:
            return get_string('state_completed_and_sent_error','plannedmail');
        default :
            return get_string('state_notstarted','plannedmail');
    }
}

function plannedmail_cm_info_view(cm_info $cm){
    global $DB;
    
    $plannedmail = $DB->get_record('plannedmail', array('id'=>$cm->instance));
    $course = get_course($cm->course);
    
    $content = plannedmail_get_information_string($plannedmail, $course, $cm);
    
    $cm->set_content($content);
}

function plannedmail_get_information_string($plannedmail, $course, $cm) {
    global $DB, $PAGE;
    
    $out = mb_substr($plannedmail->mailcontent,0, 500);
    
    $maildelay_week = floor($plannedmail->maildelay/604800);
    $maildelay_day = floor(($plannedmail->maildelay%604800)/86400);
    $maildelay_hour = floor(($plannedmail->maildelay%86400)/3600);
    $maildelay_minute = floor(($plannedmail->maildelay%3600)/60);
    
    $maildelay_array = array();
    
    if ($maildelay_week>0){
        $maildelay_array[] = get_string('maildelay_week'.($maildelay_week>1?'s':''), 'plannedmail', $maildelay_week);
    }
    if ($maildelay_day>0){
        $maildelay_array[] = get_string('maildelay_day'.($maildelay_day>1?'s':''), 'plannedmail', $maildelay_day);
    }
    if ($maildelay_hour>0){
        $maildelay_array[] = get_string('maildelay_hour'.($maildelay_hour>1?'s':''), 'plannedmail', $maildelay_hour);
    }
    if ($maildelay_minute>0){
        $maildelay_array[] = get_string('maildelay_minute'.($maildelay_minute>1?'s':''), 'plannedmail', $maildelay_minute);
    }
    
    $delay = implode(', ', $maildelay_array);
    
    $timesent = '';
    if ($plannedmail->mailtime > 0) {
        $timesent = userdate($plannedmail->mailtime, get_string('strftimetime24', 'langconfig'));
    }
    
    $issuerstr = get_string('emiter_notification', 'plannedmail');
    if ($plannedmail->issuerid > 0) {
        $issueruser = $DB->get_record('user', array('id'=>$plannedmail->issuerid));
        if ($issueruser !== false) {
            $issuerurl = new moodle_url('/user/view.php', array('id'=>$issueruser->id, 'course'=>$course->id));
            $issuerstr = html_writer::link($issuerurl, $issueruser->firstname.' '.$issueruser->lastname);
        }else{
            $issuerstr = get_string('emiter_unknown', 'plannedmail');
        }
    }
    
    $out .= '<br><br>'.get_string('emiterdelay'.($plannedmail->mailtime>0?'_hour':''), 'plannedmail', array(
        'issuer' => $issuerstr,
        'delay' => $delay,
        'hour' => $timesent
    ));
    
    $modinfo = get_fast_modinfo($course);
    $cm = $modinfo->get_cm($cm->id);
    $info = new \core_availability\info_module($cm);
    $fullinfo = $info->get_full_information();
    if ($fullinfo) {
        $formattedinfo = \core_availability\info::format_info(
            $fullinfo, $course);
        $renderer = $PAGE->get_renderer('core','course');
        $out .= $renderer->availability_info($formattedinfo);
    }
    // $output = $PAGE->get_renderer('core','availability');
    // echo $output->render($fullinfo);
    return $out;
}
