# Moodle plannedmail module
- Source Code: https://gitlab.com/dne-elearning/moodle-magistere/moodle-mod_plannedmail
- License: http://www.gnu.org/licenses/gpl-3.0.html

## Install from an archive
- Extract the archive in the /mod/plannedmail folder
- Install by connecting to your moodle as an administrator or user the CLI script **admin/cli/upgrade.php** if you have access to a console.

## Description

Send message to students using access restriction ! 

This activity module is heavily inspired by the plugin https://moodle.org/plugins/mod_reengagement made by Catalyst IT. It's fairly similar with some modifications to better suits some specifc use case.

Features :
- Use access restriction to determine when to start the timer before sending the message.
- Choose the sender of the message : noreply or a specific teacher in the course.
- Configure the sending mode : Moodle notification or local mail message (if the plugin https://moodle.org/plugins/local_mail is available).
- Send a custom notification to the teacher each time a message is sent to a student.
- Abort the sending when a specific activity is completed.
- See the states of the plannedmail for each students.
- Two completion rules : when the message is sent and when the message delay is over 
  
## How to use

- Configure the Message mode in the planned mail settings page (only needed if the plugin local_mail is installed on your Moodle, otherwise can on only be the mode Moodle Notification). 
- Create a plannedmail instance in one of your course.
- Set the activity parameters as you like : the access restrictions are used to determine when to start the timer for the delay.
- When the access restriction is OK for a student, the timer will start. At the end of ther timer, send the message if the access restriction is OK. 

When configuring the message sent from the plugin there are limited place holders you can use. If need to use them properly wrap them with "percentage" (%) symbols. The substitutions below are available :
- %courseshortname%
- %coursefullname%
- %courseid%
- %userfirstname%
- %userlastname%
- %userid%
- %usercity%
- %userinstitution%
- %userdepartment%
- %date%
- %time%

